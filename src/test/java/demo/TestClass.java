package demo;

import org.testng.Assert;
import org.testng.annotations.Test;
import utils.CommonUtils;

public class TestClass extends BaseClass {

    @Test
    public void Activity1_MakeMyTripSearch() {
        mmtLandingPage.navigateToMakeMyTripSite();
        mmtLandingPage.goToSearchFlightPage();
        mmtLandingPage.selectFromCity("Ahmedabad");
        mmtLandingPage.selectToCity("Bengaluru");
        mmtLandingPage.selectDepart(2);
        mmtLandingPage.searchFlight();
        String price = mmtLandingPage.getFlightPrice(0);
        Assert.assertEquals(price, "5271", "Mismatch the price of the airlines");
    }

    @Test
    public void Activity2_Trivago() {
        trivagoDashboardPage.openNewTab();
        trivagoDashboardPage.navigateToTrivagoSite();
        trivagoDashboardPage.selectHotelName("Ahmedabad");
        trivagoDashboardPage.selectDate("2021-07-08");
        trivagoDashboardPage.selectDate("2021-07-09");
        trivagoDashboardPage.selectAdults(3);
        trivagoDashboardPage.selectChildren(2);
        trivagoDashboardPage.selectRooms(2);
        trivagoDashboardPage.selectApplyButton();
        trivagoDashboardPage.selectSearchButton();
        CommonUtils.takeSnapShot(driver);
    }
}
