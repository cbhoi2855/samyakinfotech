package utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class CommonUtils {

    private static SimpleDateFormat tsdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    public static void takeSnapShot(WebDriver driver) {
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            String imageName = "screenshot_" + tsdf.format(timestamp) + ".jpg";
            String dest = System.getProperty("user.dir") + "//screenshots//";
            File dir = new File(dest);
            if (!dir.exists()) {
                dir.mkdir();
            }

            String filePath = dest + imageName;
            File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenshot, new File(filePath));
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
