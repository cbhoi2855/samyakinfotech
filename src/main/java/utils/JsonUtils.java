package utils;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;

public class JsonUtils {

    static String srcPath = System.getProperty("user.dir");
    static String templatePath = "//src//main//resources//templates";
    public static String readJsonFile(String fileName){
        JSONParser jsonParser = new JSONParser();
        JSONObject obj = null;
        if(!fileName.startsWith("/")){
            fileName = "/"+fileName;
        }
        String filename = srcPath+templatePath+fileName;
        try (FileReader reader = new FileReader(filename)) {
            //Read JSON file
            obj = (JSONObject) jsonParser.parse(reader);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return obj.toString();
    }
}
