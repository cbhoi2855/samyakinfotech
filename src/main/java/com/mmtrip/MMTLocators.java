package com.mmtrip;

import org.openqa.selenium.By;

public interface MMTLocators {
    By displayBlock = By.xpath("//*[contains(@class,'loginModal displayBlock modalLogin dynHeight personal')]");
    By btnSearchLanding = By.xpath("//a[text()='Search']");
    By lblFromCity = By.id("fromCity");
    By txtFromCity = By.xpath("//input[@placeholder='Enter City']");
    By txtToCity = By.xpath("//input[@placeholder='Enter City']");
    By btnSearchFlight = By.id("search-button");
    By iconOverlayCross = By.xpath("//span[@class='bgProperties icon20 overlayCrossIcon']");
    By listElementFlightsPrices = By.xpath("//div[@class='priceSection']");
}
