package com.mmtrip;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class MMTLandingPage implements MMTLocators{
    WebDriver driver;
    String baseURL;

    public MMTLandingPage(WebDriver driver, String baseURL) {
        this.driver = driver;
        this.baseURL = baseURL;
    }

    private String convertPriceToString(String price){
        return price.split(" ")[1].replace(",", "");
    }

    public void navigateToMakeMyTripSite() {
        driver.get(baseURL);
        if (isElementPresent(displayBlock)) {
            driver.findElement(displayBlock).click();
        }
    }

    public void goToSearchFlightPage(){
        driver.findElement(btnSearchLanding).click();
    }

    private boolean isElementPresent(By elementLocator) {
        boolean isPresent = false;
        try {
            Thread.sleep(1000);
            if (driver.findElement(elementLocator).isDisplayed()) {
                isPresent = true;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return isPresent;
    }


    public void selectFromCity(String fromCityName){
        driver.findElement(lblFromCity).click();
        driver.findElement(txtFromCity).sendKeys(fromCityName);
        String serchFromXpath = "//span[contains(text(),'" + fromCityName + "')]";
        driver.findElement(By.xpath(serchFromXpath)).click();
    }

    public void selectToCity(String toCityName){
        driver.findElement(txtToCity).sendKeys(toCityName);
        String serchToXpath = "//span[contains(text(),'" + toCityName + "')]";
        driver.findElement(By.xpath(serchToXpath)).click();
    }

    public void selectDepart(int dayOffSet){
        By dpNext2Days = By.xpath("//div[contains(@class,'--selected')]//following::div["+dayOffSet+"]");
        driver.findElement(dpNext2Days).click();
    }

    public void searchFlight(){
        driver.findElement(btnSearchFlight).click();
        if (isElementPresent(iconOverlayCross)) {
            driver.findElement(iconOverlayCross).click();
        }
    }

    public String getFlightPrice(int flightRowNumber){
        List<WebElement> listPrice = driver.findElements(listElementFlightsPrices);
        String price = listPrice.get(flightRowNumber).findElement(By.xpath("//div[@class='priceSection']//p")).getText();
        return convertPriceToString(price);
    }

    public void searchFlite() {
        String searchFromCity = "Ahmedabad";
        String searchToCity = "Bengaluru";
        driver.get(baseURL);










    }
}
