package com.trivago;

import org.openqa.selenium.By;

public interface TrivagoLocators {

    By plcHotelName = By.xpath("//input[@placeholder='Enter a hotel name or destination']");
    By btnCheckIn = By.xpath("//button[@key='checkInButton']");
    By btnApply = By.xpath("//div[@key='df-over']//button[text()='Apply']");
    By btnSearch = By.xpath("//button//span[text()='Search']");

}
