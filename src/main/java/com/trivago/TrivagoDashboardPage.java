package com.trivago;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;

public class TrivagoDashboardPage  implements TrivagoLocators{
    WebDriver driver;
    String baseURL;
    WebDriverWait wait;

    public TrivagoDashboardPage(WebDriver driver, String baseURL) {
        this.driver = driver;
        this.baseURL = baseURL;
        wait = new WebDriverWait(driver, 10);
    }

    public void openNewTab() {
        ((JavascriptExecutor) driver).executeScript("window.open()");
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
    }

    public void navigateToTrivagoSite() {
        driver.get(baseURL);
    }

    public void selectHotelName(String hotelName)  {
        driver.findElement(plcHotelName).sendKeys("Ahmedabad");
        pause(2000);
        driver.findElement(By.xpath("//span[@class='ssg-title']/mark[text()='" + hotelName + "']/../following::span[@class='ssg-subtitle' and contains(text(),'Hotel')]"));
        driver.findElement(btnCheckIn).click();
    }

    public void selectDate(String date_yyyy_mm_dd) {
        driver.findElement(By.xpath("//div[@class='two-month-calendar']//time[@datetime='" + date_yyyy_mm_dd + "']")).click();
    }

    public void selectAdults(int noOfAdults) {
        String type = "Adults";
        String typeXpath = "//div[@class='room-filters']//label[text()='" + type + "']/..//div";
        if (noOfAdults != 2) {
            if (noOfAdults > 2) {
                int count = noOfAdults - 2;
                String plusBtn = typeXpath + "//button[@class='circle-btn circle-btn--plus']";
                for (int i = 1; i <= count; i++) {
                    driver.findElement(By.xpath(plusBtn)).click();
                }
            } else {
                int count = 2 - noOfAdults;
                String minusBtn = typeXpath + "//button[@class='circle-btn circle-btn--minus']";
                for (int i = 1; i <= count; i++) {
                    driver.findElement(By.xpath(minusBtn)).click();
                }
            }
        }
    }

    public void selectChildren(int noOfChild) {
        String type = "Children";
        String typeXpath = "//div[@class='room-filters']//label[text()='" + type + "']/..//div";
        if (noOfChild > 0) {
            String plusBtn = typeXpath + "//button[@class='circle-btn circle-btn--plus']";
            for (int i = 1; i <= noOfChild; i++) {
                driver.findElement(By.xpath(plusBtn)).click();
            }
        }
    }

    public void selectRooms(int noOfRoom) {
        String type = "Rooms";
        String typeXpath = "//div[@class='room-filters']//label[text()='" + type + "']/..//div";
        if (noOfRoom > 1) {
            String plusBtn = typeXpath + "//button[@class='circle-btn circle-btn--plus']";
            for (int i = 1; i < noOfRoom; i++) {
                driver.findElement(By.xpath(plusBtn)).click();
            }
        }
    }

    private void pause(int milis) {
        try {
            Thread.sleep(milis);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void selectApplyButton() {
        driver.findElement(btnApply).click();
    }

    public void selectSearchButton() {
        driver.findElement(btnSearch).click();
    }
}
